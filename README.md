# Yum RPM repository

This repository contains rpm's to be installed on CentOS so they can be hosted centrally for easier access.

## Contributing

You can contribute and work with this repository with Git en Git-LFS. See https://confluence.atlassian.com/bitbucket/use-git-lfs-with-bitbucket-828781636.html for more info
